# Lehigh University CSE262 - Programming Languages - Homework 5

Solve the following ten questions regarding the Lambda Calculus.


λxy.x(λz.zy)yyλz.xy(xz)




## Question 1

Make all parentheses explicit in these λ- expressions:

1. (λp.pz) λq.w λw.wqzp

(λp.(pz)) (λq.(w (λw.(((wq)z)p))))



2. λp.pq λp.qp

λp.((pq) (λp.(qp)))


## Question 2

In the following expressions say which, if any, variables are bound (and to which λ), and which are free.

1. λs.s z λq.s q

λs.((s z) (λq.(s q)))
s is bound to λs
q is bound to λq
z is free in λs
s is free in λq

2. (λs. s z) λq. w λw. w q z s

(λs. (s z)) (λq. (w (λw. (((w q) z) s))))
s is bound to λs
z is free from λs
w is free from λq
w is bound to λw
q is bound to λq
z is free from λq
s is free from λq

3. (λs.s) (λq.qs)

(λs.s) (λq.(qs))
s is bound by λs
q is bound by λq
s is free from λq

4. λz. (((λs.sq) (λq.qz)) λz. (z z))

λz. (((λs.(sq)) (λq.(qz))) (λz. (z z)))
s is bound by λs
q is free
q is bound by λq
z is bound by λz
z is bound by λz
z is bound by λz

## Question 3

Put the following expressions into beta normal form (use β-reduction as far as possible, α-conversion as needed) assuming left-association.

1. (λz.z) (λq.q q) (λs.s a)

((λz.z) (λq.(q q))) (λs.(s a))

(q q) [q -> (λz.z)] (λs.(s a))

((λz.z)(λz.z)) (λs.(s a))

(z) [z ->(λz.z)] (λs.(s a))

(λz.z) (λs.(s a))



2. (λz.z) (λz.z z) (λz.z q)

(((λz.z) ((λz.z) z))) ((λz.z) q))

(z) [z -> (λz.z)]

(((λz.z)z)((λz.z)q)

(z) [z -> (λz.z)]

(z((λz.z)q))

(q) [q -> (λz.z)]

(z q)

3. (λs.λq.s q q) (λa.a) b

(((((λs.(λq.s)) q) q) (λa.a)) b)

(s) [s ->(λq.q)]

((((λq.q)q)(λa.a))b)

(q) [q - >(λq.q)]

((q(λa.a))b)


4. (λs.λq.s q q) (λq.q) q

(((((λs.(λq.s)) q ) q) (λq.q)) q )

(s) [s - >(λq.q)]

((((λq.q)q) (λq.q))q)

(q) [q -> (λq.q)]

((q(λq.q))q)

5. ((λs.s s) (λq.q)) (λq.q)

((((λs.s) s) (λq.q)) (λq.q))

(s) [s -> (λq.q)]

((s(λq.q))(λq.q))

## Question 4

1. Write the truth table for the or operator below.

Input A | Input B | OUTPUT
T         T         T
T         F         T
F         T         T
F         F         F


2. The Church encoding for OR = (λp.λq.p p q)

Prove that this is a valid "or" function by showing that its output matches the truth table above. You will have 4 derivations. For the first derivation, show the long-hand solution (don't use T and F, use their definitions). For the other 3 you may use the symbols in place of the definitions. 

true = (λa. λb. a)
false = (λa. λb. b)

or true true = (λp.λq.p p q) (λa. λb. a) (λa. λb. a) = (λa. λb. a) (λa. λb. a) (λa. λb. a) = true

or true false = (λp.λq.p p q) (λa. λb. a) (λa. λb. b) = (λa. λb. a) (λa. λb. a) (λa. λb. b) = (λa. λb. a) = true

or false true = (λp.λq.p p q) (λa. λb. b) (λa. λb. a) = (λa. λb. b) (λa. λb. b) (λa. λb. a) = (λa. λb. a) = true

or false false = (λp.λq.p p q) (λa. λb. b) (λa. λb. b) = (λa. λb. b) (λa. λb. b) (λa. λb. b) = true

## Question 5

Derive a lambda expression for the NOT operator. Explain how this is similar to an IF statement.

NOT = (λp.λa.λb p b a)

(λp.λa.λb p b a) (true) = (λp.λa.λb p b a) (λa. λb. a) = λa.λb (λa.λb. a) b a = λa.λb (λx. b) a = λa. λb. b = false

This is similar to an if statement because an if statement is the same thing except the b and the a are reversed.


## Instructions

1. Fork the relevant repository into your own namespace. [Instructions](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)
2. Set your forked repository visibility to private. [Instructions](https://docs.gitlab.com/ee/public_access/public_access.html#how-to-change-project-visibility)
3. Add user "LehighCSE262" as a member to the project with "maintainer" access-level. [Instructions](https://docs.gitlab.com/ee/user/project/members/#add-a-user). 
4. Clone your newly forked repository. [Instructions](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) 
5. Answer the questions here in the readme or in another document. Upload your solutions here to Gitlab.